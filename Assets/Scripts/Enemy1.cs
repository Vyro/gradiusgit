﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy1 : MonoBehaviour
{
    private GameManager gameManager;

    public Rigidbody2D rb2D;

    public float speed;
    
    // Start is called before the first frame update
    void Start()
    {
        gameManager = new GameManager();
    }

    // Update is called once per frame
    void Update()
    {
        rb2D.velocity = -transform.right * speed;
    }
    
    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Weapon")
        {
            gameManager.score += 100;
            Destroy(gameObject);
        }
    }
    
    
}
