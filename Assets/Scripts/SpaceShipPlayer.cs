﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Serialization;

public class SpaceShipPlayer : MonoBehaviour
{
    private GameManager gameManager;
    private int pv;
    
    public Transform transformWeapons;
    public GameObject[] basicBullets;
    public int bulletNumber;

    public float speedBullet = 2f;

    // Start is called before the first frame update
    void Start()
    {
        gameManager = new GameManager();
    }

    // Update is called once per frame
    void Update()
    {
        gameManager.pvPlayer = pv;

        if (Input.GetKeyDown(KeyCode.Space))
        {
            // Fire !
            Shoot();
        }
        
        if (bulletNumber >= basicBullets.Length)
        {
            bulletNumber = 0;
        }
        
        if (Input.GetKeyDown(KeyCode.E))
        {
            // Bonus !
            Debug.Log("Activation Bonus");
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            // Select
            Debug.Log("Select");
        }

        if (Input.GetKeyDown(KeyCode.Return))
        {
            // Start
            Debug.Log("Start");
        }
    }

    public void OnCollisionEnter2D(Collision2D collision)
        {
            // Fait disparaitre les tirs quand l'ennemi est touché
            if (collision.gameObject.tag == "Enemy")
            {
                pv--;
                Destroy(gameObject);
            }
        }

        void Shoot()
        {
            if (!basicBullets[bulletNumber].activeSelf)
            {
                Debug.Log("Lancement de bullet !");
                basicBullets[bulletNumber].SetActive(true);
                basicBullets[bulletNumber].transform.position = transformWeapons.position;
                basicBullets[bulletNumber].transform.rotation = transformWeapons.rotation;
                basicBullets[bulletNumber].GetComponent<Rigidbody2D>().velocity = transform.right * speedBullet;
                bulletNumber++;

                
            }
            
        }
    }