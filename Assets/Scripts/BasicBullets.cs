﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicBullets : MonoBehaviour
{
    public Rigidbody2D rb2D;
    
    // Start is called before the first frame update
    void Start()
    {
    
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        // Fait disparaitre les tirs quand l'ennemi est touché
        if (collision.gameObject.tag == "Enemy")
        {
            gameObject.SetActive(false);
        }
    }

    /*private void OnTriggerEnter2D(Collider2D other)
    {
        // Fait disparaitre les tirs quand l'ennemi est touché
        if (other.gameObject.tag == "Enemy")
        {
            gameObject.SetActive(false);
        }
    }*/

    // Fait disparaitre l'objet quand il sort de l'écran
    void OnBecameInvisible()
    {
        gameObject.SetActive(false);
    }
}
