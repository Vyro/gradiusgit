﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    public int score;
    public int highScore;
    public int pvPlayer;
    
    // Start is called before the first frame update
    void Start()
    {
        score = 0;
        pvPlayer = 3;
    }

    // Update is called once per frame
    void Update()
    {
        if (score > highScore)
        {
            highScore = score;
        }

        if (pvPlayer <= 0)
        {
            Debug.Log("Game Over !");
        }
    }
    
    void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        else if(Instance != null)
        {
            Destroy(gameObject);
        }
    }
}
