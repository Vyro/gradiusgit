﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

public class SpawnEnemies : MonoBehaviour
{
    private ObjectPooler objectPooler;
    public float timerSpawn = 0.5f;

    private void Start()
    {
        objectPooler = ObjectPooler.instance;
    }

    private void FixedUpdate()
    {
        //objectPooler.SpawnFromPool("Enemy", transform.position, Quaternion.identity);
        if (timerSpawn <= 0)
        {
            objectPooler.SpawnFromPool("Enemy", transform.position, Quaternion.identity);
            timerSpawn = 0.5f;
        }
        else
        {
            timerSpawn -= Time.deltaTime;
        }
            

    }
}
